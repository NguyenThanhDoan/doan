﻿namespace Ktraa.Models
{
	public class Transaction
	{
		public int TransactionId { get; set; }
		public DateTime TransactionDate { get; set; }
		public decimal Amount { get; set; }
		public string? TransactionType { get; set; }

		public int AccountId { get; set; }

		public Account Account { get; set; }
		public Employee Employee { get; set; }
		public virtual ICollection<Report> report { get; set; }
		public virtual ICollection<Log> log { get; set; }

	}
}
