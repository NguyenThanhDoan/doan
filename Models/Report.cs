﻿namespace Ktraa.Models
{
	public class Report
	{
		public int ReportId { get; set; }
		public string ReportName { get; set; }
		public DateTime ReportDate { get; set; }

		public int EmployeeId { get; set; }

		public Transaction? transaction { get; set; }
		public Log? log { get; set; }
		public Account? account { get; set; }

	}
}
