﻿namespace Ktraa.Models
{
	public class Log
	{
		public int LogId { get; set; }
		public DateTime LogDateTime { get; set; }
		

		public int TransactionId { get; set; }
		public int? ReportId { get; set; }

		public Transaction? Transaction { get; set; }
		

	}
}
