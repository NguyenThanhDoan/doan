﻿using Microsoft.EntityFrameworkCore;

namespace Ktraa.Models
{
	public class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext>
	   options) : base(options)
		{
		}
		public DbSet<Employee> employees { get; set; }
		
		public DbSet<Customer> customers { get; set; }
		public DbSet<Account> accounts { get; set; }
		public DbSet<Log> log { get; set; }
		public DbSet<Transaction> transactions { get; set; }
		public DbSet<Report> report { get; set; }
		
	}
}
