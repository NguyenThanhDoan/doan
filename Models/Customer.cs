﻿using System.Security.Principal;


namespace Ktraa.Models
{
	public class Customer
	{
		public int CustomerId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Contact { get; set; }
		public string Address { get; set; }
		public string UserName { get; set; }

		public string PassWord { get; set; }
		public decimal Price { get; set; }

		

	}
}
